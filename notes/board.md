# MCU board

In this project we are using the ESP32 based devkit board. The ESP32 is a series of low-cost, low-power System on a Chip (SoC) microcontrollers developed by Espressif. These modules include both WIFI and bluetooth radios and a dual-core processor, so the possibilities for developing projects around this module are endless. The board is popular among hobbyists, and if you are familiar with other Espressif chips, such as the ESP8226 then you will find this chip a much more advanced platform with lots of extra features.

We will be using the following board in this project

Front of the board

![mcu-front](pics/esp32-board-front.png)

Back of the board

![mcu-back](pics/esp32-board-back.png)

Here is a schematic showing how we commonly wire loads into GPIO pins. The below schematic is just an example and you would in practice have some specific device wiring that you would use.

![mcu](pics/mcu_bb.png)

More technical information about the MCU can be found here https://randomnerdtutorials.com/getting-started-with-esp32/

the board was designed using [kicad](https://www.kicad.org/), schematic and pcb can be downloaded from [here](https://gitlab.com/dlopenweek/iot/-/tree/main/) and they are released under CERN-OHL-S-2.0 license
