# gyroscope

The MPU6050 module is a very interesting sensor module for the hobbyist. It contains both a gyroscope and an acceleromter, which enables the measurement of rotation about all three axes (x,y and z) as well as the acceleration in their planar directions due to gravity or motion. These devices are of tremendous interest in the hobbyist world since they have application in technologies involving gesture recognition, gaming controllers, augmented and virtual reality systems, self driving vehicles, wearable fitness technology and many more.

The board we will use in this project is:

Front of the board

![mpu6050-front](pics/accelerometer-board-front.png)

Back of the board

![mpu6050-back](pics/accelerometer-board-back.png)

The MPU6050 sensor module used the I2C communication protocol so wiring this module to your microcontroller is very simple. It is recommended to lookup and use the default I2C pins on your microcontroller if you are not sure. An example of doing this with the ESP32 microcontroller:

| MPU6050  |  ESP32    |
| -------- | --------- |
| VCC      |  5V       |
| GND      |  GND      |
| SCL      |  GPIO 22  |
| SDA      |  GPIO 21  |

A simple wiring schematic showing this is below.

## schematic

![mpu6050](pics/mpu6050_bb.png)

For more in-depth technical information and examples see this link https://lastminuteengineers.com/mpu6050-accel-gyro-arduino-tutorial/
