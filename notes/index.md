# Sensor board

Our IoT sensor board for the Daresbury Open Week is a simple circuit design containing several sensors. The idea behind this project is to go beyond the Arduino workshops to show how the same ideas can be applied to create smart connected devices. 

Our project board is built around the ESP32 MCU which has WIFI and Bluetooth connectivity options, this enables us to build a device platform that can communicate with it's wider environment. Communicating with devices in the wider environment is a key requirement to building smart devices that can enable smart automation around the home or in professional settings.

An overview and more advanced information about all of the devices on board can be found at the following links:

- *[ESP32](board.md)* Microcontroller
- *[BME280](bme280.md)* Temperature, Humidity and Pressure Sensor
- *[MPU6050](gyro.md)* Gryoscope and Accelerometer Sensor
- *[RCWL-0516](rcwl.md)* Microwave Radar Motion Sensor
- *[Neopixel](neopixel.md)* RGB LED
- *[LCD Screen](screen.md)*
- *[Push Button](switch.md)* Switch

As can be seen in the above links, the list of sensors and devices above are commonly and cheaply available as circuit modules. We have designed our project board here to take advantage of this availability to construct a modular sensor project board for teaching purposes.

Our board was designed to use the footprint of these commonly available devices with the track layout being informed by the connection protocol for each sensor circuit module. The vast majority of the sensors are connected into the I2C bus pins, whilst the button, LED are connected to digital IO pins on the MCU and the LCD screen connected to the SPI bus pins.

Our circuit layout looks like this:

![circuit design](pics/pcb-layout.png)

As you can see, around the edge of the board there are much larger holes, these holes were intentionally spaced such that the circuit board can be mounted upon a lego plinth. The 3D cad views of the schematic are as follows:

Front of the board with no components:
![circuit design front no components](pics/board-3dview-no.png)

Back of the board with no components:
![circuit design back no components](pics/board-3dview-back.png)

Front of the board with CAD component models
![circuit design front with components](pics/board-3dview.png)

We then generated the gerber files for the above board and had it fabricated by a professional fabrication factory. We also soldered headers to the board and the sensor/device modules, such that they are removable and configurable and mounted onto the lego plinth.

Lego base before board is pressed on
![lego base](pics/lego-base.jpg)

Board assembled onto Lego plinth, with headers soldered onto board and components.
![circuit board with headers](pics/board-no-modules.jpg)

Board with all device components installed.
![circuit board with device components](pics/board-fully-assembled.jpg)

