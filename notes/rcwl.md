# motion sensor

The The RCWL-0516 sensor module is a motion detector sensor module. This is a popular sensor module amongst the hobbyist community since motion detection is a central idea to smart home automation and security systems. The RCWL-0516 module is different to other motion type sensors, such as PIRs since it relies on microwave reflections by measuring subtle changes in reflected signals to detect movements. PIRs, detect infra-red, so is only able to detect mainly living things, whilst this module will detect motion for a wide array of objects, such as cars. The penetrative power of microwave radiation, also allows this module to pass through some types of walls to detect the motion on the other side, so is particularly useful for covert surveilance.

We are using the following board:

Front of the board

![motion_sensor-front](pics/radar-board-front.png)

Back of the board

![motion_sensor-back](pics/radar-board-back.png)

The RCWL-0516 sensor module used the I2C communication protocol so wiring this module to your microcontroller is very simple. It is recommended to lookup and use the default I2C pins on your microcontroller if you are not sure. An example of doing this with the ESP32 microcontroller:

| BME280   |  ESP32    |
| -------- | --------- |
| Vin      |  5V       |
| GND      |  GND      |
| SCL      |  GPIO 22  |
| SDA      |  GPIO 21  |

## schematic

![motion_sensor](pics/rcwl-0516_bb.png) 

More advanced technical information as well as software libraries can be found at the following link https://lastminuteengineers.com/rcwl0516-microwave-radar-motion-sensor-arduino-tutorial/
