# Screen

Our project board has an onboard ST773S 1.8" TFT LCD screen which is the black and white variant of the LCD panel. The LCD module is capable of loading complex text and imagery, either via its data pins from a microcontroller, or directly via an onboard SD card port.

In this project we are using this module:

Front of the board

![st7735 1.8 inch tft screen](pics/lcd-screen-board-front.png)

Back of the board

![st7735 1.8 inch tft screen](pics/lcd-screen-board-back.png)

The LCD module used the SPI communication protocol so wiring this module to your microcontroller is very simple. It is recommended to lookup and use the default SPI pins on your microcontroller if you are not sure. An example of doing this with the ESP32 microcontroller:

| LCD      |  ESP32    |
| -------- | --------- |
| VCC      |  5V       |
| GND      |  GND      |
| CS       |  GPIO 5   |
| RST      |  GPIO 2   |
| A0       |  GPIO 0   |
| SDA      |  GPIO 23  |
| SCK      |  GPIO 18  |
| LED      |  3.3V     |

## schematic

![st7735 1.8 inch tft screen](pics/st7735s_bb.png)

For more technical information and an example code project see https://randomnerdtutorials.com/guide-to-1-8-tft-display-with-arduino/
