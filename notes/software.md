# Software


All software we use in this project is open source. The two main components are

- [Homeassistant](https://www.home-assistant.io/) which is the server and coordinates all the sensors and the boards

- [esphome](https://www.home-assistant.io/) which control eash individual board and makes it available to homeassistant.

Both are free of charge to use and modify and exist due to effort of a large number of volunteers.


## board software

the full configuration for the software can be found [here](https://gitlab.com/dlopenweek/iot/-/blob/main/software/mersey01.yaml). We do not explicitly program the microcontroller but we describe what we would like to do and esphome transforms our instructions in code for it. It makes it easy and fast to programme an IOT device but may not give you access to all the options of a sensor.


A quick explanation of the file you can find below.

Setup some substitutions for things which are used more than once

``` yaml

substitutions:
  devicename: mersey01
  sensor_temp: bme280
  sensor_acc: mpu6050
  update: 30s


```


!!! info "setting up the mcu"

    setting up the microcontroller involves telling esphome the name of the mcu, in this case nodemcu-32s and setting up the
    services we need for our project, wifi, logging, messaging(mqtt), etc.

``` yaml

esphome:
  name: $devicename
  platform: ESP32
  board: nodemcu-32s

wifi:
  ssid: !secret wifi
  password: !secret wifi_password

  # Enable fallback hotspot (captive portal) in case wifi connection fails
  ap:
    ssid: ${devicename} Fallback Hotspot
    password: "MLO4IA0MZb10"

captive_portal:

# Enable logging
logger:

# Enable Home Assistant API
api:
#  password: !secret ota_password
  reboot_timeout: 0s

status_led:
  pin:
     number: GPIO13
     inverted: true

mqtt:
  broker: !secret mqtt_broker
  discovery: true

ota:
  password: !secret ota_password

```

setup an rgb light, switch and motion sensor

``` yaml

light:
  - platform: neopixelbus
    type: GRB
    variant: WS2812
    pin: GPIO16
    num_leds: 1
    name: "NeoPixel Light"
    id: rgb_light

binary_sensor:
  - platform: gpio
    id: my_motion
    pin: GPIO17
    name: "radar motion sensor"
    device_class: motion
  - platform: gpio
    pin:
      number: GPIO32
      mode:
        input: true
        pullup: true
      inverted: true
    name: "Basic Button"
    on_press:
      - switch.toggle: button

switch:
  - platform: gpio
    name: "button"
    icon: "mdi:light"
    id: button
    pin: GPIO32

```

setup i2c sensors (bme280 and mpu6050)


!!! info "note!!!"

    we transform the gyroscope readings in more conventional angles that can be used in automations... check roll and pitch sections



```yaml

i2c:
  - id: bus_b
    sda: GPIO21
    scl: GPIO22
    scan: true

sensor:
  - platform: $sensor_temp
    id: my_bme
    address: 0x76
    i2c_id: bus_b
    update_interval: ${update}
    temperature:
      id: my_temp
      name: ${sensor_temp} ${devicename} Temperature
      oversampling: 16x
      accuracy_decimals: 2
    pressure:
      name: ${sensor_temp} ${devicename} Pressure
      id: my_pres
      accuracy_decimals: 2
    humidity:
      name: ${sensor_temp} ${devicename} Humidity
      id: my_hum
      accuracy_decimals: 2
  - platform: ${sensor_acc}
    address: 0x68
    update_interval: 1500ms
    id: my_acc
    accel_x:
      name: "${sensor_acc} ${devicename} Accel X"
      id: my_x
      filters:
          - sliding_window_moving_average:
              window_size: 5
              send_every: 1
          - delta: 0.5
    accel_y:
      name: "${sensor_acc} ${devicename} Accel Y"
      id: my_y
      filters:
          - sliding_window_moving_average:
              window_size: 5
              send_every: 1
          - delta: 0.5
    accel_z:
      name: "${sensor_acc} ${devicename} Accel Z"
      id: my_z
      filters:
          - sliding_window_moving_average:
              window_size: 5
              send_every: 1
          - delta: 0.5
    gyro_x:
      name: "${sensor_acc} ${devicename} Gyro X"
      id: my_gx
      filters:
          - sliding_window_moving_average:
              window_size: 5
              send_every: 1
          - delta: 0.5
    gyro_y:
      name: "${sensor_acc} ${devicename} Gyro Y"
      id: my_gy
      filters:
          - sliding_window_moving_average:
              window_size: 5
              send_every: 1
          - delta: 0.5
    gyro_z:
      name: "${sensor_acc} ${devicename} Gyro Z"
      id: my_gz
      filters:
          - sliding_window_moving_average:
              window_size: 5
              send_every: 1
          - delta: 0.5
    temperature:
      name: "${sensor_acc} ${devicename} Temperature"
  - platform: template
    id: roll
    name: roll
    accuracy_decimals: 2
    lambda: |-
      return  (atan( id(my_y).state / sqrt( pow( id(my_x).state , 2) + pow( id(my_z).state , 2) ) ) * 180 / PI) ;
    update_interval: 250ms

  - platform: template
    id: pitch
    name: pitch
    accuracy_decimals: 2
    lambda: |-
      return  (atan(-1 * id(my_x).state / sqrt(pow(id(my_y).state, 2) + pow(id(my_z).state, 2))) * 180 / PI);
    update_interval: 250ms


```


setup screen and print information coming from sensors

```yaml

font:
  - file: "droid.ttf"
    id: my_font
    size: 12

display:
  - platform: st7735
    spi_id: bus_a
    model: "INITR_18BLACKTAB"
    reset_pin: GPIO02
    cs_pin: GPIO05
    dc_pin: GPIO00
    rotation: 90
    device_width: 128
    device_height: 160
    col_start: 0
    row_start: 0
    eight_bit_color: true
    update_interval: 100ms
    lambda: |-
      it.printf(0, 24, id(my_font), "T: %.2f C",id(my_temp).state);
      it.printf(0, 36, id(my_font), "h: %.2f %%",id(my_hum).state);
      it.printf(0, 48, id(my_font), "p: %.1f hPa",id(my_pres).state);
      it.printf(0, 60, id(my_font), "x,y,z: %.1f %.1f %1.f ",id(my_x).state,id(my_y).state,id(my_z).state);
      it.printf(0, 72, id(my_font), "a(x,y,z) %.1f %.1f %1.f ",id(my_gx).state,id(my_gy).state,id(my_gz).state);
      it.printf(0, 84, id(my_font), "roll %.1f pitch %1.f ",id(roll).state,id(pitch).state);
      it.printf(0, 96, id(my_font), "motion %d ",id(my_motion).state);


```

you can browse the code for other devices used in the workshop [lamp](https://gitlab.com/dlopenweek/iot/-/blob/main/software/lamp.yaml) and [mersey00](https://gitlab.com/dlopenweek/iot/-/blob/main/software/mersey00.yaml)

